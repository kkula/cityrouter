$(function() {
	$(document).on('keypress', function(e) {
		if (e.charCode === 96) {
			// tilda was pressed
			e.preventDefault();
			$('#console').slideToggle(200);
		}
	});

	var startPoint = {
		lat: 52.2320,
		lng: 21.121,
		zoom: 12
	};


	// loading map
	var mapOptions = {
		zoom: startPoint.zoom,
		center: new google.maps.LatLng(startPoint.lat, startPoint.lng),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	document.map = new google.maps.Map(document.getElementById("map"), mapOptions);
	document.renderer = new google.maps.DirectionsRenderer({
		preserveViewport: true,
		suppressMarkers: true
	});
	document.renderer.setMap(document.map);

	var r = new CityRoute({
		position: {
			lat: 52.22927,
			lng: 21.0093
		}
	});

	google.maps.event.addListener(document.map, 'click', function(e) {
		var lat = e.latLng.lat();
		var lng = e.latLng.lng();
		r.setDestination(lat, lng);
	});



});