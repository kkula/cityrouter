
/**
 * Computes distance between two points. Objects should contain lat and lng fields.
 * @param object a
 * @param object b
 * @returns Numeric distance in meters.
 */
var distance = function(a,b) {
	return 111195 * Math.acos( (Math.sin(a.lat) * Math.sin(b.lat)) + (Math.cos(a.lat) * Math.cos(b.lat) * Math.cos(Math.abs( a.lng - b.lng )) ) );
};

var CityRoute = function(options) {
	
	this.option = options;
	this.option.step = 1;
	this.option.speed = 50; // km/h
	
	this.point = new google.maps.Marker({
		position: new google.maps.LatLng(this.option.position.lat,this.option.position.lng),
		title: 'Hello',
		draggable: true
	});
	this.point.setMap(document.map);
	
	this.destination = null;
	
	this.fn = function() {
		console.log(this.point.getPosition());
	}
	var that = this;
	setTimeout(function() {
		console.log(that);
		that.fn();
	},1000);
	
	this.simulate = function() {
		var speed = that.option.speed;
		
		var t = new Date();
		var duration = (t - that.lastCheck)/1000;
		that.lastCheck = t;
		var total_dis = duration * speed * 10 / 36;
		console.log(duration);
		
		var p = that.point.getPosition();
		p = {
			lat: p.lat(),
			lng: p.lng()
		};
		while(true) {
			if(!that.route.length)
				break;
			var x = {
				lat: that.route[0].lat(),
				lng: that.route[0].lng()
			};
			var dis = distance(p,x);
			if(dis > total_dis) {
				
				
				var df = (total_dis/dis);
				
				p.lat += df * (x.lat - p.lat);
				p.lng += df * (x.lng - p.lng);
				break;
				
			}
			total_dis -= dis;
			p = x;
			that.route.shift();
		}
		that.point.setPosition(new google.maps.LatLng(p.lat,p.lng));
		
		if(that.route.length) {
			that.fnHandler = setTimeout(that.simulate,that.option.step*1000);
		} else {
			that.fnHandler = null;
		}
		
	}
	
	this.public = {
		that: this,
		setDestination: function(lat,lng) {
	
			if(that.fnHandler) {
				clearTimeout(that.fnHandler);
				that.fnHandler = null;
			}
	
			// TODO: stop previous ride.
	
			console.log(distance({lat:that.point.getPosition().lat(),lng:that.point.getPosition().lng()},{lat:lat,lng:lng}));
			
			console.log('New Destination: ' + lat + ' ' + lng);
			console.log(that);
			that.lastCheck = new Date();
			that.destination = {
				lat: lat,
				lng: lng
			};
			that.route = null;
			var req = {
				origin: that.point.getPosition(),
				destination: new google.maps.LatLng(lat,lng),
				travelMode: google.maps.TravelMode.DRIVING
			};
			
			var service = new google.maps.DirectionsService();
			service.route(req,function(result,status) {
				console.log('Getted Route');
				console.log(result);
				if(status === google.maps.DirectionsStatus.OK) {
					// drawing
					document.renderer.setDirections(result);
					that.route = result.routes[0].overview_path;
					
					that.fnHandler = setTimeout(that.simulate,that.option.step*1000);
					
					
					
				}
			});
			
		}
	};
	
	var that = this;
	
	google.maps.event.addListener(this.point,'dragend',function() {
		that.public.setDestination(that.destination.lat,that.destination.lng);
	});
	
	return this.public;
	
};